#!/usr/bin/env python3
import random
from faker import Faker
fake = Faker('es_ES')

def create_product(client, type='product'):
    Product = client.env["product.template"]
    product_id = Product.create({
        'name': fake.sentence(nb_words=3),
        'default_code': fake.ean13(),
        'type': type,
    })
    return product_id

def random_product(client, type='product'):
    Product = client.env["res.partner"]
    domain = [('type', '=', type)]
    product_ids = Product.search(domain)
    return random.choice(product_ids)

def random_partner(client, customer=False, supplier=False):
    Partner = client.env["res.partner"]
    domain = []
    if customer:
        domain.append(("customer", "=", True))
    if supplier:
        domain.append(("supplier", "=", True))
    partner_ids = Partner.search(domain)
    return random.choice(partner_ids)

