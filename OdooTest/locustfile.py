from locust import HttpLocust, between
from OdooRPCLocust import OdooRPCLocust

from OdooAccountTasks import InvoicingUserBehavior


class InvoicingUser(OdooRPCLocust):
    wait_time = between(0.500, 5)
    weight = 10
    task_set = InvoicingUserBehavior

