#!/usr/bin/env python
from random import choice, randint

from faker import Faker
fake = Faker()

from locust import TaskSet, seq_task, task
import CommonTasks


class CommonAccountingTasks(TaskSet):
    @task(10)
    def read_partners(self, limit=80):
        Partner = self.client.env["res.partner"]
        partner_ids = Partner.search([], limit=limit)
        Partner.read(choice(partner_ids))

    @task(20)
    def read_products(self, limit=80):
        Product = self.client.env["product.product"]
        product_ids = Product.search([], limit=limit)
        Product.read(choice(product_ids))

    @task(20)
    def read_journals(self, limit=80):
        Product = self.client.env["account.journal"]
        product_ids = Product.search([], limit=limit)
        Product.read(choice(product_ids))

    @task(1)
    def stop(self):
        self.interrupt()


class InvoicingTasks(TaskSet):
    def __init__(self, *args, **kwargs):
        super(InvoicingTasks, self).__init__(*args, **kwargs)
        self.Invoice = self.client.env["account.invoice"]

    def _create_sale_invoice(self, lines=3):
        partner_id = CommonTasks.random_partner(self.client, customer=True)
        invoice_id = self.Invoice.create({
            'partner_id': partner_id,
            'type': 'out_invoice'
        })
        return invoice_id

    def _validate_invoice(self, invoice_id):
        self.Invoice.action_invoice_open([invoice_id])

    @task(10)
    def create_partner(self):
        Partner = self.client.env["res.partner"]
        partner = Partner.create({
            'name': fake.name(),
            'vat': fake.pyint(min_value=10000, max_value=99999, step=1),
        })

    @task(1)
    def create_product(self):
        product_id = CommonTasks.create_product(self.client)

    @task(2)
    def create_draft_sale_invoice(self):
        lines = randint(2,30)
        invoice_id = self._create_sale_invoice(lines=lines)

    @task(5)
    def create_open_sale_invoice(self):
        lines = randint(2,30)
        invoice_id = self._create_sale_invoice(lines=lines)
        return self._validate_invoice(invoice_id)

    @task(1)
    def stop(self):
        self.interrupt()


class UserAuth(TaskSet):
    def on_start(self):
        self.client.verify = False
        self.client.login(self.locust.db_name, self.locust.login, self.locust.password)

    def on_stop(self):
        self.client.logout()


class InvoicingUserBehavior(UserAuth):
    tasks = {
        CommonAccountingTasks: 5,
        InvoicingTasks: 2,
    }
