# ODOO TESTING WITH LOCUST

## INSTALLATION

Install required packages with:

python3 -m pip install locustio
pip3 install odoorpc
pip3 install faker

## USAGE

You must run the service in the folder you have you OdooRPCLocust.py file, to run a a service you can use the following options:

ODOO DB NAME - the database name to run the test on.

ODOO_LOGIN - login for the admin user or the user you want to run the test.

ODOO_PASSWORD - login for the admin user or the user you want to run the test.

ODOO_VERSION - The Odoo version you are working with.

locust - locust command 

--host=server - host where your database is located

:port - the port to access, only required if different that 80


The final format will look like this:

ODOO_DB_NAME="database" ODOO_LOGIN="admin" ODOO_PASSWORD="admin_password" ODOO_VERSION="x.0" locust --host=server:port

Some examples:

ODOO_DB_NAME="mydb" ODOO_LOGIN="admin" ODOO_PASSWORD="securepass" ODOO_VERSION="12.0" locust --host=http://example.com

ODOO_DB_NAME="accountdb" ODOO_LOGIN="accountant" ODOO_PASSWORD="account123" ODOO_VERSION="9.0" locust --host=https://www.example.com:8069

